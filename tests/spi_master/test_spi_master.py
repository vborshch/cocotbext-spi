#!/usr/bin/env python

import itertools
import logging
import os
import random

import cocotb_test.simulator

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge, Timer
from cocotb.regression import TestFactory

from cocotbext.spi import SpiMaster


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
class TB(object):
    def __init__(self, dut, clkrate=1e6):
        self.dut = dut

        self.log = logging.getLogger("cocotb.tb")
        self.log.setLevel(logging.DEBUG)

        self.source = SpiMaster(dut.mosi, dut.miso, dut.sclk, dut.csn, clkrate=clkrate, bits=8, mode="00")


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test(dut, payload_lengths=None, payload_data=None, slave_num=0):
    tb = TB(dut)
    await Timer(1, 'us')

    for test_data in [payload_data(x) for x in payload_lengths()]:
        tb.source.bits = len(test_data)*8 # because bytes

        await tb.source.write(int.from_bytes(test_data, byteorder='big'), slave_num)

        rx_data = await tb.source.read()
        rx_data = rx_data[0]

        tb.log.info("Readed data: %s", rx_data)

        assert tb.source.empty_read()
        assert rx_data == test_data

        await Timer(1, 'us')
    await Timer(100, 'ns')

    return True


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def prbs31(state=0x7fffffff):
    while True:
        for i in range(8):
            if bool(state & 0x08000000) ^ bool(state & 0x40000000):
                state = ((state & 0x3fffffff) << 1) | 1
            else:
                state = (state & 0x3fffffff) << 1
        yield state & 0xff


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def prbs_payload(length):
    gen = prbs31()
    return bytearray([next(gen) for x in range(length)])


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def incrementing_payload(length):
    return bytearray(itertools.islice(itertools.cycle(range(256)), length))


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def size_list():
    return list(range(1, 16)) + [32, 64, 128]


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
# cocotb-test
if cocotb.SIM_NAME:
    factory = TestFactory(run_test)
    factory.add_option("payload_lengths", [size_list])
    factory.add_option("payload_data", [incrementing_payload])
    factory.add_option("slave_num", [0, 1, 2])
    factory.generate_tests()
