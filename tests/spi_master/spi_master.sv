module spi_master
#(
  parameter int CSN_N = 1
)
(
  input  bit              mosi ,
  output bit              miso ,
  input  bit              sclk ,
  input  bit  [CSN_N-1:0] csn
);

assign miso = mosi;

endmodule

