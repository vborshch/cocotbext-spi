module spi_slave
#(
  parameter int CSN_N = 1
)
(
  inout wire              mosi ,
  inout wire              miso ,
  inout wire              sclk ,
  inout wire  [CSN_N-1:0] csn
);

endmodule

