# SPI interface modules for Cocotb

## Introduction

SPI simulation models for [cocotb](https://github.com/cocotb/cocotb).

## Installation

Installation from pip (release version, stable):

    - Unavailable

Installation from gitlab:

    $ git clone https://gitlab.com/vborshch/cocotbext-spi
    $ pip install -e cocotbext-spi

## Documentation and usage examples

See the `tests` directory for complete testbenches using these modules.

### SPI

The `SpiMaster` and `SpiSlave` classes can be used to drive, receive, and monitor serial periphey intertface modules.

To use these modules, import the one you need and connect it to the DUT:

    from cocotbext.spi import SpiMaster, SpiSlave

    self.master = SpiMaster(dut.mosi, dut.miso, dut.sclk, dut.csn, clkrate=clkrate, bits=8, mode="00")
    self.slave = SpiSlave(dut.mosi, dut.miso, dut.sclk, dut.csn, bits=8, mode="00")

To send data into a design with a `SpiMaster`, call `write()` or `write_nowait()`.  Accepted data types are iterables of ints, including lists, bytes, bytearrays, etc.  Optionally, call `wait()` to wait for the transmit operation to complete.  Example:

    await master.send(b'test data')
    # wait for operation to complete (optional)
    await master.wait()

Also SpiMaster can read data back from SpiSlave with methods `read()` or `read_nowait()`.

To receive data with a `SpiSlave`, call `read()` or `read_nowait()`.  Optionally call `wait()` to wait for new receive data.  `read()` will block until at least 1 data byte is available.  Both `read()` and `read_nowait()` will return up to _count_ bytes from the receive queue, or the entire contents of the receive queue if not specified.

    data = await slave.read()

Also SpiSlave can write data to SpiMaster with methods `write()` or `write_nowait()`.

#### Constructor parameters:

* _mosi_: Master Out Slave In signal
* _miso_: Master In Slave Out signal
* _sclk_: Master clock
* _csn_ : Chip Select negative
* _clkrate_: clock rate in herz (optional, default 1e6, only for master)
* _bits_: bits per word (optional, default 8)
* _mode_: SPI mode (optional, default "00") *Not implemented yet*

#### Attributes:

* _clkrate_: clock rate in herz
* _bits_: bits per word

#### Methods

* `write(data)`: send _data_ (blocking) (all)
* `write_nowait(data)`: send _data_ (non-blocking) (all)
* `read(count)`: read _count_ bytes from buffer (blocking) (all)
* `read_nowait(count)`: read _count_ bytes from buffer (non-blocking) (all)
* `count_read()`: returns the number of items in the read queue (all)
* `empty_read()`: returns _True_ if the read queue is empty (all)
* `count_write()`: returns the number of items in the write queue (all)
* `empty_write()`: returns _True_ if the write queue is empty (all)
* `idle()`: returns _True_ if no transfer is in progress or if the queue is not empty (all)
* `clear()`: drop all data in queue (all)
* `wait()`: wait for idle (all)
* `wait(timeout=0, timeout_unit='ns')`: wait for data received (all)
