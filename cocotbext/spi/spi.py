"""
Copyright (c) 2021 Vladislav Borshch
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import logging
from collections import deque

import cocotb
from cocotb.triggers import FallingEdge, RisingEdge, Timer, First, Event

from .version import __version__


class SpiMaster:
    def __init__(self, mosi, miso, sclk, csn, clkrate=1e6, bits=8, mode="00", *args, **kwargs):
        self.log = logging.getLogger(f"cocotb.{mosi._path}")
        self._mosi = mosi
        self._miso = miso
        self._sclk = sclk
        self._csn = csn
        self._clkrate = clkrate
        self._bits = bits
        self._mode = mode

        self.log.info("SPI master")
        self.log.info("cocotbext-spi version %s", __version__)
        self.log.info("Copyright (c) 2021 Vladislav Borshch")
        self.log.info("https://gitlab.com/vborshch/cocotbext-spi")

        super().__init__(*args, **kwargs)

        self.active = False
        self.queue_write = deque()
        self.queue_read = deque()
        self.sync_write = Event()
        self.sync_read = Event()

        self._idle = Event()
        self._idle.set()

        self.log.info("SPI master configuration:")
        self.log.info("  Clock rate: %d Hz", self._clkrate)
        self.log.info("  Word size: %d bits", self._bits)
        self.log.info("  Mode: %s", self._mode)
        self.log.info("  CSn number: %d", len(self._csn))

        # Push all CSn to logic one
        self._csn.setimmediatevalue(2**len(self._csn)-1)

        self._run_cr = None
        self._restart()

    def _restart(self):
        if self._run_cr is not None:
            self._run_cr.kill()
        self._run_cr = cocotb.fork(self._run(self._mosi, self._miso, self._sclk, self._csn,
                                             self._clkrate, self._bits, self._mode)
        )

    @property
    def clkrate(self):
        return self._clkrate

    @clkrate.setter
    def clkrate(self, value):
        self.clkrate = value
        self._restart()

    @property
    def bits(self):
        return self._bits

    @bits.setter
    def bits(self, value):
        self._bits = value
        self._restart()

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, value):
        self.mode = value
        self._restart()

    async def write(self, data, csn_num):
        self.write_nowait(data, csn_num)

    def write_nowait(self, data, csn_num):
        self.queue_write.append([data, csn_num])
        self.sync_write.set()
        self._idle.clear()

    async def read(self, count=-1):
        while self.empty_read():
            self.sync_read.clear()
            await self.sync_read.wait()
        return self.read_nowait(count)

    def read_nowait(self, count=-1):
        if count < 0:
            count = len(self.queue_read)
        data = []
        for k in range(count):
            data.append(self.queue_read.popleft())
        return data

    def count_read(self):
        return len(self.queue_read)

    def empty_read(self):
        return not self.queue_read

    def clear_read(self):
        self.queue_read.clear()

    def count_write(self):
        return len(self.queue_write)

    def empty_write(self):
        return not self.queue_write

    def idle_write(self):
        return self.empty_write() and not self.active

    def clear_write(self):
        self.queue_write.clear()

    async def wait(self):
        if not self.empty_write():
            return
        self.sync_write.clear()
        await self._idle.wait()

    async def _run(self, mosi, miso, sclk, csn, clkrate, bits, mode):
        self.active = False

        half_bit_t = Timer(int(1e9/self.clkrate/2), 'ns')

        while True:
            while not self.queue_write:
                self.active = False
                self._idle.set()
                self.sync_write.clear()
                await self.sync_write.wait()

            rxword = []
            [word, csn_num] = self.queue_write.popleft()
            self.active = True

            self.log.info("Write word 0x%X to slave %d", word, csn_num)

            # Pull down clock signal
            sclk <= 0
            # Pull down one of CSn
            csn[csn_num] <= 0
            await half_bit_t

            # data bits and clock signal
            for _ in range(bits):
                # writing
                mosi <= word & 1
                word >>= 1
                # reading
                await half_bit_t
                rxword.append(str(miso.value.integer & 1))
                # clock
                sclk <= 1
                await half_bit_t
                await half_bit_t
                sclk <= 0
                await half_bit_t

            # last tick bit
            sclk <= 0
            csn[csn_num] <= 1
            await half_bit_t
            await half_bit_t

            # Reverse LSB-MSB in received word and join to one string
            rxword = "".join(rxword[::-1])
            # Convert to bytearray
            rxword = bytes(int(rxword[i:i+8], 2) for i in range(0, len(rxword), 8))

            self.log.info("Readed word 0x%X from slave %d", rxword, csn_num)
            self.queue_read.append(rxword)
            self.sync_read.set()

            self.active = False




class SpiSlave:
    def __init__(self, mosi, miso, sclk, csn, bits=8, mode="00", *args, **kwargs):
        self.log = logging.getLogger(f"cocotb.{mosi._path}")
        self._mosi = mosi
        self._miso = miso
        self._sclk = sclk
        self._csn = csn
        self._bits = bits
        self._mode = mode

        self.log.info("SPI slave")
        self.log.info("cocotbext-spi version %s", __version__)
        self.log.info("Copyright (c) 2021 Vladislav Borshch")
        self.log.info("https://gitlab.com/vborshch/cocotbext-spi")

        super().__init__(*args, **kwargs)

        self.active = False
        self.queue_write = deque()
        self.queue_read = deque()
        self.sync_write = Event()
        self.sync_read = Event()

        self._idle = Event()
        self._idle.set()

        self.log.info("SPI slave configuration:")
        self.log.info("  Word size: %d bits", self._bits)
        self.log.info("  Mode: %s", self._mode)

        self._miso.setimmediatevalue(0)

        self._run_cr = None
        self._restart()

    def _restart(self):
        if self._run_cr is not None:
            self._run_cr.kill()
        self._run_cr = cocotb.fork(self._run(self._mosi, self._miso, self._sclk, self._csn, self._bits, self._mode))

    @property
    def bits(self):
        return self._bits

    @bits.setter
    def bits(self, value):
        self._bits = value
        self._restart()

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, value):
        self.mode = value
        self._restart()

    async def write(self, data):
        self.write_nowait(data)

    def write_nowait(self, data):
        self.queue_write.append(data)
        self.sync_write.set()
        self._idle.clear()

    async def read(self, count=-1):
        while self.empty_read():
            self.sync_read.clear()
            await self.sync_read.wait()
        return self.read_nowait(count)

    def read_nowait(self, count=-1):
        if count < 0:
            count = len(self.queue_read)
        data = []
        for k in range(count):
            data.append(self.queue_read.popleft())
        return data

    def count_read(self):
        return len(self.queue_read)

    def empty_read(self):
        return not self.queue_read

    def clear_read(self):
        self.queue_read.clear()

    def count_write(self):
        return len(self.queue_write)

    def empty_write(self):
        return not self.queue_write

    def idle_write(self):
        return self.empty_write() and not self.active

    def clear_write(self):
        self.queue_write.clear()

    async def wait(self):
        if not self.empty_write():
            return
        self.sync_write.clear()
        await self._idle.wait()

    async def _run(self, mosi, miso, sclk, csn, bits, mode):
        self.active = False

        while True:
            # Waiting for line active
            await FallingEdge(csn)
            self.active = True

            rxword = []
            tx_ena = False

            # If we have something to sent to master
            if self.queue_write:
                word = self.queue_write.popleft()
                tx_ena = True
                self.log.info("Write word 0x%X to master", word)

            # data bits and clock signal
            for _ in range(bits):
                # writing
                if tx_ena:
                    miso <= word & 1
                    word >>= 1
                # reading
                await RisingEdge(sclk)
                rxword.append(str(mosi.value.integer & 1))
                await FallingEdge(sclk)

            # last tick bit
            await RisingEdge(csn)

            # Reverse LSB-MSB in received word and join to one string
            rxword = "".join(rxword[::-1])
            # Convert to bytearray
            rxword = bytes(int(rxword[i:i+8], 2) for i in range(0, len(rxword), 8))

            self.log.info("Readed word 0x%X from master", rxword)
            self.queue_read.append(rxword)
            self.sync_read.set()

            self.active = False
